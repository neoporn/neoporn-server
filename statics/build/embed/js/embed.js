let totalTimeViewed = 0
var incTime
const path = window.location.pathname
const pathName = path.split('/')

fetch(`http://localhost:8081/api/getvideo/?videoId=${encodeURIComponent(pathName[2])}`)
.then(video => {
    video.json().then(function(data) {
        let video2 = document.createElement('video')
        let source = document.createElement('source')
        video2.id = "video2"
        video2.style.width = "100%"
        video2.style.height = "auto"
        video2.controls = true
        video2.controlsList = "nodownload"
        source.src = data.video.url
        source.type = "video/mp4"
        
        video2.appendChild(source)
        document.getElementById('root').appendChild(video2)
        
        video2.addEventListener('play', () => {
            if (totalTimeViewed < 15) {
                incTime = setInterval(() => {
                    if (totalTimeViewed === 15) {
                        clearInterval(incTime)
                        fetch(`http://localhost:8081/api/incv`, {
                            method: "PUT",
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                              },
                            body: JSON.stringify({ t: totalTimeViewed, videoId: pathName[2] })
                        })
                    }
                    else totalTimeViewed++
                }, 1000)
            }
        })
        video2.addEventListener('pause', () => {
            clearInterval(incTime)
        })
      });
})