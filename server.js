// Dependencies
require('dotenv').config()
const redirectToHttps = require('./redirectToHttps')
const express = require('express')
const parseurl = require('parseurl')
const bodyParser = require('body-parser')
const path = require('path')
const expressValidator = require('express-validator')
const mongoose = require('mongoose')
const url = `mongodb://localhost:27017/neoporn`
const fileUpload = require('express-fileupload')
const bcrypt = require('bcryptjs')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken')
const emailValidator = require('./my_modules/email_validator/emailValidator')
const app = express()
const fs = require('fs')
const mkdirp = require('mkdirp-promise')
const http = require('http')
const https = require('https')
const axios = require('axios')
const socketIO = require('socket.io')
const sharp = require("sharp")
const multipart = require('connect-multiparty')
const { forEach } = require('p-iteration')
var resumable = require('./resumable-node.js')((__dirname + "/public/uploads"))
var router = express.Router()
const mailgun = require('mailgun-js')({ apiKey: "d4aedcbf843b49d91d95efe21747abd9-e44cc7c1-53f5dd1a", domain: "mg.intimy.shop" })
const { spawn } = require('child_process')
const pid = process.pid

const env = process.env.NODE_ENV || 'development'

// Models
const membersModel = require('./models/membersModel.js')
const videosModel = require('./models/videosModel.js')
const followsModel = require('./models/followsModel.js')
const likesModel = require('./models/likesModel.js')
const dislikesModel = require('./models/dislikesModel.js')
const contactModel = require('./models/contactModel.js')
const notificationsModel = require('./models/notificationsModel.js')
const commentsModel = require('./models/commentsModel.js')
const wallUserProfile = require('./models/wallUserProfile.js')
const favoritesModel = require('./models/favoritesModel.js')

app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname + '/statics'))

app.use(multipart())

let server

var options = {
  key: fs.readFileSync("keys/private.key"),
  cert: fs.readFileSync("keys/__neoporn_net.crt"),
  ca: [
    fs.readFileSync('keys/__neoporn_net.ca-bundle')
  ]
}

if (env === "development") {
  server = http.createServer(app)
}

else {
  server = https.createServer(options, app)
  app.use((req, res, next) => {
    const host = req.header("host")
    if (host.match(/^www\..*/i)) {
      next()
    } else {
      res.redirect(301, "https://www." + host + req.url)
    }
  })
  redirectToHttps()
}

const io = socketIO(server)

app.use(function (req, res, next) {
  let origin = req.headers.origin
  let originAllowed = ["https://neoporn.net", "https://www.neoporn.net", "https://transcoding.neoporn.net", "https://www.transcoding.neoporn.net", "http://localhost:8082", "http://localhost:8081", "http://localhost:3000"]
  if (originAllowed.indexOf(origin) != -1) {
    res.setHeader("Access-Control-Allow-Origin", origin)
  }
  res.header("Access-Control-Allow-Credentials", true)
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization')
  res.header("Access-Control-Max-Age", "1728000")
  if (req.method == "OPTIONS") {
    res.sendStatus(200)
    res.end()
  }
  else {
    next()
  }
})

app.use(bodyParser.urlencoded({ limit: "50mb", extended: false }))
app.use(bodyParser.json({ limit: "50mb" }))
app.use(cookieParser())


app.post('/api/postvideo', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    if (req.body.fields.title) {
      membersModel.findOne({ _id: decoded.userId })
        .then(member => {
          let videoData = {}
          let path = "members/" + decoded.userId + "/videos/" + req.body.file
          videoData.title = req.body.fields.title
          videoData.description = req.body.fields.description
          videoData.tags = req.body.fields.tags
          videoData.uploader = decoded.userId
          videoData.uploaderName = member.username
          videoData.url = `${process.env.URL_TRANSCODING_SERVER}/${path}.mp4`,
            videoData.fileName = req.body.file
          let video = new videosModel(videoData)
          video.save()
            .then(async dataVideo => {
              const followers = await followsModel.find({ followedId: decoded.userId })
              const followersId = await followers.map(follower => {
                const notification = {
                  notifType: 'followedNewVideo',
                  userId: follower.followerId,
                  followedNewVideo: {
                    videoId: dataVideo._id,
                    uploaderId: decoded.userId
                  }
                }
                const newNotification = new notificationsModel(notification)
                newNotification.save()
              })
              let pathThumbnail = "members/" + decoded.userId + "/videos/thumbnails/" + dataVideo._id + "/img_05.jpg"
              videosModel.updateOne({ _id: dataVideo._id }, { $set: { urlThumbnail: `${process.env.URL_TRANSCODING_SERVER}/${pathThumbnail}` } }).exec()
              axios.post(`${process.env.URL_TRANSCODING_SERVER}/api/transcodevideo`, { dataVideo, member, file: req.body.file })
              res.send("Ok")
            })
        })
    }
  })
})


app.post('/api/notifyMe', function (req, res) {
  if (req.body.id && req.body.emailAddress) {
    if (req.body.id != '1' && req.body.id != '2') {
      res.send('You are not allowed to do that!')
    }
    else {
      emailListModel.findOne({ emailAddress: req.body.emailAddress })
        .then(data => {
          if (data) {
            res.send('Already exist')
          }
          else if (!data) {
            let emailAddress = new emailListModel(req.body)
            emailAddress.save()
            res.send('Ok')
          }
        })
    }
  }
  else {
    res.send('A field is missing')
  }
})

app.get('/api/members', (req, res) => {
  membersModel.find({}).then(eachOne => {
    res.json(eachOne)
  })
})

app.post('/api/addbio', (req, res) => {
  req.body.aboutMe = req.body.aboutMe.trim()
  jwt.verify(req.cookies.authtoken2, 'supersecret', (err, decoded) => {
    membersModel.updateOne({ '_id': decoded.userId }, { aboutMe: req.body.aboutMe }).then(member => {
      res.send("Ok")
    })
  })
})

app.post('/api/addlink', (req, res) => {
  if (req.body.targetLink) {
    const targetLink = req.body.targetLink.trim().toString().toLowerCase()
    const links = `links.${req.body.target}`
    jwt.verify(req.cookies.authtoken2, 'supersecret', (err, decoded) => {
      membersModel.updateOne({ '_id': decoded.userId }, { [links]: targetLink }).then(member => {
        res.send("Ok")
      })
    })
  }
  else {
    res.send("Empty")
  }
})

app.post('/api/removelinkslink', (req, res) => {
  if (req.body.target) {
    const target = `links.${req.body.target}`
    jwt.verify(req.cookies.authtoken2, 'supersecret', (err, decoded) => {
      membersModel.updateOne({ '_id': decoded.userId }, { $unset: { [target]: "" } }).then(member => {
        res.send("Ok")
      })
    })
  }
  else {
    res.send("Empty")
  }
})

app.get('/api/getprofildata', (req, res) => {
  membersModel.findOne({ '_id': req.query.userId }).then(eachOne => {
    res.json(eachOne)
  })
})

app.get('/api/getmyvideos', async (req, res) => {
  try {
    const videos = await videosModel.find({ 'uploader': req.query.userId, authorized: true })
      .skip(parseInt(req.query.skipSize))
      .limit(9)
      .sort({ date: -1 })
    const totalLength = await videosModel.countDocuments({ 'uploader': req.query.userId, authorized: true })
    res.json({ videos, totalLength })
  } catch (error) {
    console.log(error)
  }
})

app.get('/api/getmyfavoritesvideos', async (req, res) => {
  try {
    const favorites = await favoritesModel.find({ 'userId': req.query.userId }).select("videoId")
    const videosIds = await favorites.map(id => id.videoId)
    const videos = await videosModel.find({ '_id': { $in: videosIds }, authorized: true })
      .skip(parseInt(req.query.skipSize))
      .limit(9)
      .sort({ date: -1 })
    const totalLength = await videosModel.countDocuments({ '_id': { $in: videosIds }, authorized: true })
    res.json({ videos, totalLength })
  } catch (error) {
    console.log(error)
  }
})

app.get('/api/getmyfavorites', async (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', async function (err, decoded) {
    const favorites = await favoritesModel.find({ userId: decoded.userId, videoId: req.query.videoId })
    if (favorites.length > 0) {
      res.json({ favorite: true })
    }
    else res.json({ favorite: false })
  })
})

app.post('/api/addfavorites', async (req, res) => {
  if (req.body.videoId) {
    jwt.verify(req.cookies.authtoken2, 'supersecret', async function (err, decoded) {
      const member = await membersModel.findOne({ _id: decoded.userId }).select("username")
      favoritesModel.findOne({ userId: decoded.userId, videoId: req.body.videoId })
        .then(data => {
          if (data) {
            data.remove()
              .then(() => {
                res.send('Favorite removed')
              })
          }
          else {
            let favorite = {}
            favorite.videoId = req.body.videoId
            favorite.userId = decoded.userId
            favorite.userUsername = member.username
            favorite.userIp = req.connection.remoteAddress
            const newFavorite = new favoritesModel(favorite)
            newFavorite.save()
              .then(() => {
                res.send('Favorite added')
              })
          }
        })
    })
  }
  else {
    res.send('Allo?')
  }
})

app.get('/api/getuploadervideos', async (req, res) => {
  try {
    const videos = await videosModel.find({ 'uploader': req.query.userId, authorized: true })
      .skip(parseInt(req.query.skipSize))
      .limit(9)
      .sort({ date: -1 })
    const totalLength = await videosModel.countDocuments({ 'uploader': req.query.userId, authorized: true })
    res.json({ videos, totalLength })
  } catch (error) {
    console.log(error)
  }
})

app.get('/api/isverifieduser', (req, res) => {
  membersModel.findOne({ '_id': req.query.userId })
    .select('verifiedUser')
    .then(user => {
      if (user.verifiedUser === true) {
        res.json('User verified')
      }
      else {
        res.json('User not verified')
      }
    })
})

app.get('/api/getUsername', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', (err, decoded) => {
    membersModel.findOne({ '_id': decoded.userId })
      .select('username')
      .then(eachOne => {
        res.json(eachOne)
      })
  })
})

app.get('/api/getuploaderdata', (req, res) => {
  membersModel.findOne({ _id: req.query.userId })
    .then(user => {
      res.send(user)
    })
})

app.get('/api/getHomeCards', async (req, res) => {
  try {
    if (req.query.idAlreadyGot) {
      let filter = { authorized: true, _id: { $not: { $in: req.query.idAlreadyGot } } }
      let options = { limit: 24, date: -1 }
      let videos = await new Promise((resolve, reject) =>
        videosModel.findRandom(filter, {}, options, (err, result) => {
          if (err) reject(err)
          else resolve(result)
        }))
      let totalLength = await videosModel.countDocuments({ authorized: true })
      res.json({ videos, totalLength })
    }
    else {
      let filter = { authorized: true }
      let options = { limit: 24, date: -1 }
      let videos = await new Promise((resolve, reject) =>
        videosModel.findRandom(filter, {}, options, (err, result) => {
          if (err) reject(err)
          else resolve(result)
        }))
      let totalLength = await videosModel.countDocuments({ authorized: true })
      res.json({ videos, totalLength })
    }

  } catch (err) {
    console.log(err)
  }
})

app.get('/api/getsearchresult', async (req, res) => {
  try {
    if (req.query.searchString) {
      if (req.query.idAlreadyGot) {
        let filter = { $text: { $search: req.query.searchString }, authorized: true, _id: { $not: { $in: req.query.idAlreadyGot } } }
        let options = { limit: 24, date: -1 }
        let videos = await new Promise((resolve, reject) =>
          videosModel.findRandom(filter, {}, options, (err, result) => {
            if (err) reject(err)
            else resolve(result)
          }))
        let totalLength = await videosModel.countDocuments({ authorized: true, $text: { $search: req.query.searchString } })
        res.json({ videos, totalLength })
      }
      else {
        let filter = { $text: { $search: req.query.searchString }, authorized: true }
        let options = { limit: 24, date: -1 }
        let videos = await new Promise((resolve, reject) =>
          videosModel.findRandom(filter, {}, options, (err, result) => {
            if (err) reject(err)
            else resolve(result)
          }))
        let totalLength = await videosModel.countDocuments({ authorized: true, $text: { $search: req.query.searchString } })
        res.json({ videos, totalLength })
      }
    }
  } catch (err) {
    console.log(err)
  }
})


app.get('/api/gettagresult', async (req, res) => {
  try {
    if (req.query.tag) {
      if (req.query.idAlreadyGot) {
        let filter = { $text: { $search: req.query.tag }, authorized: true, _id: { $not: { $in: req.query.idAlreadyGot } } }
        let options = { limit: 24, date: -1 }
        let videos = await new Promise((resolve, reject) =>
          videosModel.findRandom(filter, {}, options, (err, result) => {
            if (err) reject(err)
            else resolve(result)
          }))
        let totalLength = await videosModel.countDocuments({ authorized: true, $text: { $search: req.query.tag } })
        res.json({ videos, totalLength })
      }
      else {
        let filter = { $text: { $search: req.query.tag }, authorized: true }
        let options = { limit: 24, date: -1 }
        let videos = await new Promise((resolve, reject) =>
          videosModel.findRandom(filter, {}, options, (err, result) => {
            if (err) reject(err)
            else resolve(result)
          }))
        let totalLength = await videosModel.countDocuments({ authorized: true, $text: { $search: req.query.tag } })
        res.json({ videos, totalLength })
      }
    }
  } catch (err) {
    console.log(err)
  }
})

app.get('/api/getrecommendedvideos', (req, res) => {
  try {
    videosModel.aggregate([{ $match: { authorized: true } }, { $sample: { size: 10 } }])
      .then(data => {
        res.send(data)
      })
  } catch (err) {
    console.log(err)
  }
})

app.get('/api/getvideo', (req, res) => {
  try {
    videosModel.findOne({ _id: req.query.videoId, authorized: true })
      .then(video => {
        if (video) {
          membersModel.findOne({ _id: video.uploader })
            .then(member => {
              res.json({ video, member })
            })
        }
        else {
          res.send('Nothing')
        }
      })
  } catch (err) {
    console.log(err)
  }
})

app.get('/api/getvideosverif', (req, res) => {
  try {
    videosModel.find({ verified: false, authorized: false, transcoded: true })
      .then(video => {
        res.send(video)
      })
  } catch (err) {
    console.log(err)
  }
})

app.post('/api/transcodedtrue', (req, res) => {
  videosModel.updateOne({ _id: req.body.id }, { $set: { transcoded: true } }).exec()
  res.send("Ok")
})

app.put('/api/videoverif', async (req, res) => {
  const member = await membersModel.findOne({ _id: req.body.userId })
  if (req.body.validation.selectedOption === 'yes') {
    videosModel.updateOne({ _id: req.body.videoId }, { $set: { verified: true, authorized: true } })
      .then(() => {
        const notification = {
          notifType: 'verificationVideo',
          userId: member._id,
          verificationVideo: {
            videoId: req.body.videoId,
            stateVerification: true
          }
        }
        const newNotification = new notificationsModel(notification)
        newNotification.save()
        let data = {
          from: 'Neoporn <videos@neoporn.net>',
          to: `${member.email}`,
          subject: 'Neoporn video approuved',
          html: `Hello ${member.username}, Congratulations! <br /> Your video was approuved and is now online! <br /> <a href="${process.env.ORIGIN}/nomorevideosstatus/${member._id}" style="color:grey;margin-top:100px;display:block;text-decoration:none;">I don't want to receive emails anymore</a>`
        }
        if (member.emailsNotifications === true) {
          mailgun.messages().send(data, function (error, body) {
            console.log(body)
          })
        }
        res.send("Ok")
      })
  }
  else if (req.body.validation.selectedOption === 'no') {
    videosModel.findByIdAndUpdate({ _id: req.body.videoId }, { $set: { verified: true, authorized: false } })
      .then(video => {
        const notification = {
          notifType: 'verificationVideo',
          userId: member._id,
          verificationVideo: {
            videoId: req.body.videoId,
            stateVerification: false,
            reason: req.body.validation.reason
          }
        }
        const newNotification = new notificationsModel(notification)
        newNotification.save()
        let data = {
          from: 'Neoporn <videos@neoporn.net>',
          to: `${member.email}`,
          subject: 'Neoporn video rejected',
          html: `Hello ${member.username}, <br /> Oooh! We are sorry to tell you that your video was rejected because: ${req.body.validation.reason}<br />`
        }
        if (member.emailsNotifications === true) {
          mailgun.messages().send(data, function (error, body) {
            console.log(body)
          })
        }
        axios.post(`${process.env.URL_TRANSCODING_SERVER}/api/videorejected`, { memberId: member._id, fileName: video.fileName })
        res.send("No")
      })
  }
})

app.put('/api/nomorevideosstatus', (req, res) => {
  membersModel.findOne({ _id: req.body.id })
    .then(member => {
      if (member.emailsNotifications === false) {
        res.send('Already disabled')
      }
      else {
        member.emailsNotifications = false
        member.save()
          .then(() => {
            res.send('Disabled')
          })
      }
    })
})

app.get('/api/searchForm', (req, res) => {
  const parsedQuery = JSON.parse(req.query.dataSearchForm)
  let newQuery = {}
  if (parsedQuery.username) newQuery.username = parsedQuery.username
  membersModel.find(newQuery)
    .then(data => {
      if (!data.length) {
        res.send("No data")
      }
      else {
        res.send(data)
      }
    })
})

app.put('/api/notificationViewed', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    notificationsModel.update({ userId: decoded.userId }, { $set: { seen: true } }, { multi: true })
      .then(data => {
        res.send("Notif viewed")
      })
      .catch(err => { throw err })
  })
})

app.put('/api/deleteitem', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', (err, decoded) => {
    produitsSchema.remove({ _id: req.body.itemId })
      .exec()
      .then(data => {
        res.send('Item deleted')
      })
      .catch(err => { throw err })
  })
})

app.put('/api/incv', (req, res) => {
  if (req.body.t && req.body.t === 15) {
    videosModel.updateOne({ _id: req.body.videoId }, { $inc: { views: 1 } })
      .then(data => {
        res.send('Ok')
      })
  }
})

app.get('/api/getvotes', async (req, res) => {
  if (req.query.videoId) {
    const likes = await likesModel.find({ videoId: req.query.videoId })
    const dislikes = await dislikesModel.find({ videoId: req.query.videoId })
    res.json({ likes, dislikes })
  }
  else {
    res.send('No video id')
  }
})

app.get('/api/getallvotes', async (req, res) => {
  const likes = await likesModel.find()
  const dislikes = await dislikesModel.find()
  res.json({ likes, dislikes })
})

app.post('/api/vote', (req, res) => {
  if (req.body.videoId && req.body.vote) {
    jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
      if (req.body.vote === "likes") {
        likesModel.find({ videoId: req.body.videoId, userId: decoded.userId })
          .then(data => {
            if (Array.isArray(data) && data.length > 0) {
              videosModel.updateOne({ _id: req.body.videoId }, { $inc: { likes: -1 } })
                .exec()
              likesModel.deleteOne({ videoId: req.body.videoId, userId: decoded.userId })
                .then(() => {
                  res.send('Removed')
                })
            }
            else {
              dislikesModel.find({ videoId: req.body.videoId, userId: decoded.userId })
                .then(dataDislike => {
                  if (Array.isArray(dataDislike) && dataDislike.length > 0) {
                    dislikesModel.deleteOne({ videoId: req.body.videoId, userId: decoded.userId })
                      .exec()
                    videosModel.updateOne({ _id: req.body.videoId }, { $inc: { likes: 1, dislikes: -1 } })
                      .exec()
                    let newLike = {}
                    newLike.userId = decoded.userId
                    newLike.userIp = req.connection.remoteAddress
                    newLike.videoId = req.body.videoId
                    if (req.body.userId) newLike.userId = req.body.userId
                    const like = new likesModel(newLike)
                    like.save()
                      .then(() => {
                        res.send('AL RD')
                      })
                  }
                  else {
                    videosModel.updateOne({ _id: req.body.videoId }, { $inc: { likes: 1 } })
                      .exec()
                    let newLike = {}
                    newLike.userId = decoded.userId
                    newLike.userIp = req.connection.remoteAddress
                    newLike.videoId = req.body.videoId
                    if (req.body.userId) newLike.userId = req.body.userId
                    const like = new likesModel(newLike)
                    like.save()
                      .then(() => {
                        res.send('Added')
                      })
                  }
                })
            }
          })
      }
      else if (req.body.vote === "dislikes") {
        dislikesModel.find({ videoId: req.body.videoId, userId: decoded.userId })
          .then(data => {
            if (Array.isArray(data) && data.length > 0) {
              videosModel.updateOne({ _id: req.body.videoId }, { $inc: { dislikes: -1, likes: 1 } })
                .exec()
              dislikesModel.deleteOne({ videoId: req.body.videoId, userId: decoded.userId })
                .then(() => {
                  res.send('Removed')
                })
            }
            else {
              likesModel.find({ videoId: req.body.videoId, userId: decoded.userId })
                .then(dataLike => {
                  if (Array.isArray(dataLike) && dataLike.length > 0) {
                    likesModel.deleteOne({ videoId: req.body.videoId, userId: decoded.userId })
                      .exec()
                    videosModel.updateOne({ _id: req.body.videoId }, { $inc: { dislikes: 1, likes: -1 } })
                      .exec()
                    let newDislike = {}
                    newDislike.userId = decoded.userId
                    newDislike.userIp = req.connection.remoteAddress
                    newDislike.videoId = req.body.videoId
                    if (req.body.userId) newDislike.userId = req.body.userId
                    const dislike = new dislikesModel(newDislike)
                    dislike.save()
                      .then(() => {
                        res.send('AD RL')
                      })
                  }
                  else {
                    videosModel.updateOne({ _id: req.body.videoId }, { $inc: { dislikes: 1 } })
                      .exec()
                    let newDislike = {}
                    newDislike.userId = decoded.userId
                    newDislike.userIp = req.connection.remoteAddress
                    newDislike.videoId = req.body.videoId
                    if (req.body.userId) newDislike.userId = req.body.userId
                    const dislike = new dislikesModel(newDislike)
                    dislike.save()
                      .then(() => {
                        res.send('Added')
                      })
                  }
                })
            }
          })
      }
      else {
        res.send('Allo?')
      }
    })
  }
  else {
    res.send('Allo?')
  }
})

app.get('/api/getfollowers', async (req, res) => {
  if (req.query.followedId) {
    const followers = await followsModel.find({ followedId: req.query.followedId })
    res.json({ followers })
  }
  else {
    res.send('No followed id')
  }
})

app.get('/api/getmyfollowers', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', async function (err, decoded) {
    const followersData = await followsModel.find({ followedId: decoded.userId })
    const followingData = await followsModel.find({ followerId: decoded.userId })
    const followersId = await followersData.map(follower => follower.followerId)
    const followingId = await followingData.map(following => following.followedId)
    const followers = await membersModel.find({ _id: followersId })
    const following = await membersModel.find({ _id: followingId })
    res.json({ followers, following })
  })
})

app.post('/api/follow', (req, res) => {
  if (req.body.videoId && req.body.followedId) {
    jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
      followsModel.find({ followedId: req.body.followedId, followerId: decoded.userId })
        .then(data => {
          if (Array.isArray(data) && data.length > 0) {
            membersModel.updateOne({ _id: decoded.userId }, { $inc: { following: -1 } }).exec()
            membersModel.updateOne({ _id: req.body.followedId }, { $inc: { followers: -1 } }).exec()
            followsModel.deleteOne({ followedId: req.body.followedId, followerId: decoded.userId })
              .then(() => {
                res.send('Unfollowed')
              })
          }
          else {
            if (req.body.followedId != decoded.userId) {
              membersModel.updateOne({ _id: decoded.userId }, { $inc: { following: 1 } }).exec()
              membersModel.updateOne({ _id: req.body.followedId }, { $inc: { followers: 1 } }).exec()
              let follow = {}
              follow.videoId = req.body.videoId
              follow.followedId = req.body.followedId
              follow.followerId = decoded.userId
              follow.followerIp = req.connection.remoteAddress
              const newFollow = new followsModel(follow)
              newFollow.save()
                .then(() => {
                  res.send('Followed')
                })
            }
            else {
              res.send('Cannot follow yourself')
            }
          }
        })
    })
  }
  else {
    res.send('Allo?')
  }
})

app.get('/api/getcomments', function (req, res) {
  if (req.query.videoId) {
    commentsModel.find({ videoId: req.query.videoId }).sort({ date: -1 })
      .then(comments => {
        res.json(comments)
      })
  }
})

app.post('/api/addcomment', function (req, res) {
  if (req.body.videoId, req.body.comment) {
    jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
      membersModel.findOne({ _id: decoded.userId })
        .then(member => {
          let comment = {}
          comment.videoId = req.body.videoId
          comment.userUsername = member.username
          comment.userId = decoded.userId
          comment.userIp = req.connection.remoteAddress
          comment.comment = req.body.comment
          const newComment = new commentsModel(comment)
          newComment.save()
            .then(data => {
              res.json({ status: "Comment added", comment: data })
            })
        })
    })
  }
})

app.post('/api/signin', function (req, res) {
  if (!req.body.email || !req.body.password) {
    res.send('A required field is missing')
  }
  req.body.email = req.body.email.trim()
  req.body.password = req.body.password.trim()
  try {
    membersModel.findOne({ $or: [{ email: { $regex: "^" + req.body.email + "$", $options: "i" } }, { username: { $regex: "^" + req.body.email + "$", $options: "i" } }] })
      .select('password role emailVerified')
      .then(member => {
        if (member) {
          bcrypt.compare(req.body.password, member.password).then(result => {
            if (result == false) {
              res.send('Wrong password')
            }
            else {
              if (member.emailVerified === true) {
                jwt.sign({ userId: member._id, role: member.role }, 'supersecret', async (err, token) => {
                  if (err) throw err
                  await membersModel.updateOne({ _id: member._id }, { $set: { lastConnection: Date.now() } })
                  res.cookie('authtoken2', token).send('Ok')
                })
              }
              else {
                res.send('Confirm email address')
              }
            }
          })
        }
        if (!member) {
          res.send('No account founded')
        }
      })
  } catch (error) {
    throw error
  }
})

app.post('/api/signup', function (req, res) {
  req.body.email = req.body.email.trim()

  if (!req.body.email || !req.body.username || !req.body.password || !req.body.retypePassword) {
    res.send('A required field is missing')
  }
  else if (Number.isNaN(req.body.age)) {
    res.send('Age value must be a number')
  }
  else if (req.body.retypePassword !== req.body.password) {
    res.send('Passwords do not match')
  }
  else if (req.body.password.length < 6) {
    res.send('Password too short')
  }
  else if (!emailValidator.validateEmail(req.body.email)) {
    res.send('Invalid email address format')
  }
  else if (!req.body.termsConditions) {
    res.send('You have to accept the terms and conditions')
  }
  else {
    try {
      membersModel.findOne({ $or: [{ email: req.body.email }, { username: req.body.username }] })
        .select('email username')
        .then((member) => {
          if (member) {
            if (req.body.email === member.email) {
              res.send('This email address already exist')
            }
            else if (req.body.username === member.username) {
              res.send('This username already exist')
            }
          }
          if (!member) {
            req.body.username = req.body.username.trim()
            req.body.password = req.body.password.trim()
            req.body.userIp = req.connection.remoteAddress

            bcrypt.hash(req.body.password, 10, function (err, hash) {
              req.body.password = hash
              let newMember = new membersModel(req.body)
              newMember.save()
                .then(async newMemberData => {
                  let data = {
                    from: 'Neoporn <account@neoporn.net>',
                    to: `${req.body.email}`,
                    subject: 'Neoporn account activation',
                    html: `Hello ${req.body.username}, Your Neoporn account has been successfully created. Click the button below to activate it and get started. <br /> <button style="padding:10px;background-color:#9C27B0;"><a href="${process.env.ORIGIN}/activation/${newMemberData._id}" style="color:white;text-decoration:none;">Activate now</a></button>`
                  }
                  mailgun.messages().send(data, function (error, body) {
                    console.log(body)
                  })
                  res.send('Ok')
                })
                .catch(err => { throw err })
            })
          }
        })
    } catch (error) {
      throw error
    }
  }
})

app.put('/api/accountactivation', (req, res) => {
  membersModel.findOne({ _id: req.body.id })
    .then(member => {
      if (member.emailVerified === true) {
        res.send('Account already activated')
      }
      else {
        member.emailVerified = true
        member.save()
          .then(() => {
            res.send('Account activated')
          })
      }
    })
})

//Add post on user's profile
app.post('/api/addpost', function (req, res) {
  if (req.body.userId, req.body.post) {
    jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
      membersModel.findOne({ _id: decoded.userId })
        .then(member => {
          let post = {}
          post.userId = req.body.userId
          post.posterUsername = member.username
          post.posterId = decoded.userId
          post.userIp = req.connection.remoteAddress
          post.posterIp = req.connection.remoteAddress
          post.post = req.body.post
          const newPost = new wallUserProfile(post)
          newPost.save()
            .then(data => {
              res.json({ status: "Post added", post: data })
            })
        })
    })
  }
})

//Get posts on user's profile
app.get('/api/getposts', function (req, res) {
  if (req.query.userId) {
    wallUserProfile.find({ userId: req.query.userId })
      .then(posts => {
        res.json(posts)
      })
  }
})

//Reset password request, send an email with the link
app.get('/api/forgotpassword', (req, res) => {
  membersModel.findOne({ email: req.query.email })
    .then(async member => {
      if (member) {
        const secret = await member.password + "-" + member.date.getTime()
        jwt.sign({ userId: member._id }, secret, async (err, token) => {
          if (err) throw err
          let data = {
            from: "Neoporn <resetpassword@neoporn.net>",
            to: req.query.email,
            subject: 'Neoporn reset password',
            html: `You asked to reset your password <br /> Click the link below to reset your password <br /> <a href="${process.env.ORIGIN}/resetmypassword/${member._id}/${token}">Reset my password</a> `
          }
          mailgun.messages().send(data, function (error, body) {
            console.log(body)
          })
          res.send('Ok')
        })
      }
      else {
        res.send('No account found')
      }
    })
})

//Check if the token is valid to authorize or not the user to change his password
app.get('/api/authresetmypassword', (req, res) => {
  membersModel.findOne({ _id: req.query.id })
    .then(async member => {
      if (member) {
        const secret = await member.password + "-" + member.date.getTime()
        jwt.verify(req.query.resetToken, secret, async (err, token) => {
          if (err) {
            res.send("Not ok")
          }
          else {
            res.send('Ok')
          }
        })
      }
      else {
        res.send('Not ok')
      }
    })
})

//Reset the password
app.post('/api/resetmypassword', async (req, res) => {
  if (req.body.forgotPassword.newPassword === req.body.forgotPassword.confirmNewPassword) {
    try {
      var newEncryptedPassword = await bcrypt.hash(req.body.forgotPassword.newPassword, 10)
    } catch (error) {
      console.log(error)
    }
    membersModel.updateOne({ _id: req.body.id }, { password: newEncryptedPassword })
      .then(data => {
        if (data.nModified === 1) {
          res.send("Ok")
        }
        else if (data.nModified === 0) {
          res.send("Error")
        }
      })
  }
  else {
    res.send("Passwords doesn't match")
  }
})

app.put('/api/modifyMyProfile', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    membersModel.findById(decoded.userId, async (err, member) => {
      let errorExtProfilPicture
      if (req.body.username) member.username = req.body.username
      if (req.body.profilPicture) {
        await forEach(req.body.profilPicture, async profilPicture => {
          let ext = profilPicture.split(';')[0].match(/jpeg|png/)
          if (ext) {
            let data = profilPicture.replace(/^data:image\/\w+;base64,/, "")
            let buffer = new Buffer.from(data, 'base64')
            let timestamp = new Date().getTime().toString()
            let randomInt = Math.floor((Math.random() * 100) + 1)
            let path = "members/" + decoded.userId + "/profilPictures/" + timestamp + randomInt + "." + ext
            await mkdirp("public/members/" + decoded.userId + "/profilPictures/")
            await sharp(buffer)
              .rotate()
              .resize(400, 600)
              .max()
              .jpeg({ quality: 10 })
              .png({ compressionLevel: 9 })
              .toFile('public/' + path)
            member.profilPicture = `${process.env.ORIGIN}/${path}`
          }
          else {
            errorExtProfilPicture = true
          }
        })
      }
      member.save()
        .then(() => {
          if (errorExtProfilPicture) res.send("Wrong ext")
          else res.send("Profil updated!")
        })
    })
  })
})

app.put('/api/modifymyitem/:itemId', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    let updatedItem = {}
    if (req.body.name) updatedItem.name = req.body.name
    if (req.body.description) updatedItem.description = req.body.description
    if (req.body.price) updatedItem.price = req.body.price
    updatedItem.notif = req.body.notif
    if (req.body.img) {
      let ext = req.body.img.split(';')[0].match(/jpeg|png|gif/)[0]
      let data = req.body.img.replace(/^data:image\/\w+;base64,/, "")
      let buffer = new Buffer.from(data, 'base64')
      let timestamp = new Date().getTime().toString()
      let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + "." + ext
      mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/", err => {
        if (err) throw err
        fs.writeFile('public/' + path, buffer, err => {
          if (err) throw err
        })
      })
      updatedItem.img = `${process.env.ORIGIN}/${path}`
    }
    produitsSchema.update({ _id: req.params.itemId }, { $set: updatedItem }, (err, produit) => {
      if (err) throw err
      res.send("Item updated!")
    })
  })
})

app.get('/api/getnotifications', (req, res) => {
  jwt.verify(req.cookies.authtoken2, 'supersecret', async (err, decoded) => {
    const notifications = await notificationsModel.find({ userId: decoded.userId })
    const notificationsVideo = await notificationsModel.find({ userId: decoded.userId, notifType: "verificationVideo" })
    const notificationsNewVideo = await notificationsModel.find({ userId: decoded.userId, notifType: "followedNewVideo" })
    const notificationVideo = await notificationsVideo.map(data => data.verificationVideo.videoId)
    const videos = await videosModel.find({ _id: { $in: notificationVideo } })
    const newVideoMap = await notificationsNewVideo.filter(newVideo => newVideo.userId == decoded.userId)
    const uploaderNewVideos = await newVideoMap.map(data => data.followedNewVideo.uploaderId)
    const uploaderNewVideo = await membersModel.find({ _id: uploaderNewVideos })
    res.json({ notifications, videos, notificationsVideo, uploaderNewVideo })
  })
})

app.post('/api/contact', function (req, res) {
  if (req.body.email && req.body.subject && req.body.message) {
    let contactData = {}
    contactData.userEmail = req.body.email
    contactData.subject = req.body.subject
    contactData.message = req.body.message
    contactData.userIp = req.connection.remoteAddress
    const contact = new contactModel(contactData)
    contact.save()
      .then(() => {
        let data = {
          from: `${req.body.email}`,
          to: 'intimy.shop@gmail.com',
          subject: 'Neoporn contact new message',
          html: `${req.body.message}`
        }
        mailgun.messages().send(data, function (error, body) {
          console.log(body)
        })
        res.send('Message sent')
      })
  }
})

app.get('/api/authtoken2', function (req, res) {
  jwt.verify(req.cookies.authtoken2, 'supersecret', function (err, decoded) {
    res.json({ token: decoded, status: true })
  })
})

mongoose.connect(url, { useNewUrlParser: true }, function (err, db) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err)
  } else {
    console.log('Connection established to', url)
  }
})

app.get('/embed/:id', (req, res) => {
  res.sendFile(path.join(__dirname + '/statics/build/embed/html/embed.html'))
})

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname + '/statics/build/index.html'))
})

const port = process.env.PORT || 8081

server.listen(port, (req, res) => {
  console.log(`Started process ${pid}`);
})