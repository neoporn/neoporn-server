const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    comment: {
        type: String,
        required: true
    },
    videoId: {
        type: String,
        required: true
    },
    userUsername: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    userIp: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
}, { usePushEach: true })

const model = mongoose.model('Comment', schema)

module.exports = model