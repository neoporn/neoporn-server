const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    post: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    posterUsername: {
        type: String,
        required: true
    },
    posterId: {
        type: String,
        required: true
    },
    userIp: {
        type: String,
        required: true
    },
    posterIp: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
}, { usePushEach: true })

const model = mongoose.model('WallUserProfile', schema)

module.exports = model