const mongoose = require('mongoose')
const random = require('mongoose-simple-random-fork')

let Schema = mongoose.Schema

const schema = new Schema({
  title: {
    type: String,
    required: true,
    text: true
  },
  description: {
    type: String,
    text: true
  },
  views: {
    type: Number,
    default: 0
  },
  likes: {
    type: Number,
    default: 0
  },
  dislikes: {
    type: Number,
    default: 0
  },
  url: {
    type: String,
    required: true
  },
  urlThumbnail: {
    type: String,
    required: false
  },
  uploader: {
    type: String,
    required: true 
  },
  uploaderName: String,
  fileName: String,
  tags: {
    type: [String]
  },
  verified: {
    type: Boolean,
    default: false
  },
  authorized: {
    type: Boolean,
    default: false
  },
  transcoded: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

schema.index({ title: 'text', description: 'text', tags: 'text'})

schema.plugin(random)

const model = mongoose.model('Video', schema)

module.exports = model