const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  videoId: {
    type: String,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  userIp: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Dislike', schema)

module.exports = model