const mongoose = require('mongoose')

let Schema = mongoose.Schema

const links = new Schema({
  website: String,
  facebook: String,
  twitter: String,
  instagram: String
})

const schema = new Schema({
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  profilPicture: String,
  password: {
    type: String,
    required: true
  },
  aboutMe: String,
  links: links,
  role: {
    type: Number,
    default: 0
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  emailsNotifications: {
    type: Boolean,
    default: true
  },
  followers: {
    type: Number,
    default: 0
  },
  following: {
    type: Number,
    default: 0
  },
  lastConnection: {
    type: Date,
    default: Date.now
  },
  lastActivity: Date,
  userIp: {
    type: String,
    required: false
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Member', schema)

module.exports = model