const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  videoId: {
    type: String,
    required: true
  },
  followedId: {
    type: String,
    required: true
  },
  followerId: {
    type: String,
    required: true
  },
  followerIp: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Follow', schema)

module.exports = model