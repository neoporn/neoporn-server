const jwt = require('jsonwebtoken')

exports.verifyJWTToken = function (token) 
{
  return new Promise((resolve, reject) =>
  {
    jwt.verify(token, "secretsupersecret", (err, decodedToken) => 
    {
      if (err || !decodedToken)
      {
        return reject(err)
      }

      resolve(decodedToken)
    })
  })
}